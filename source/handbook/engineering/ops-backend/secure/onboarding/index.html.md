---
layout: markdown_page
title: "Secure Team Onboarding"
---

## On this page
{:.no_toc}

- TOC
{:toc}

### Overview
The features provided by the Secure Team are mostly leveraging tools that are executed during pipelines, using Docker images.
That’s why it’s crucial to set up a development environment with the [Gitlab CI Runner](https://gitlab.com/gitlab-org/gitlab-runner).

### 1. Install GDK
Install GDK (Use instructions in [Getting started section](https://gitlab.com/gitlab-org/gitlab-development-kit/blob/master/README.md#getting-started)). This process is done in two steps. 
Firstly, set up all needed dependencies, and then install GDK. 

_Note:_ Secure Team's primary target is the Gitlab Enterprise Edition. By default, GDK installs the Community Edition, 
so [follow these instructions](https://gitlab.com/gitlab-org/gitlab-development-kit/blob/master/doc/set-up-gdk.md#gitlab-enterprise-edition) to install the Enterprise Edition with GDK. 
However, you may need Community Edition for some contribution (if you alter database schema). 
As well, it could be useful to set up two GDKs side-by-side, one for CE and one for EE, as you might need to work on both for the same feature.

If you’re stuck, ask for help in `#development`, `#gdk` Slack channels. Use Slack search for your questions first with filter `in:#gdk`. There is a possibility that someone has already had a similar issue.
Alternatively, you can install [gitlab-compose-kit](https://gitlab.com/ayufan/gitlab-compose-kit) if you’re familiar with Docker. 
It makes it easy to switch back and forth between GitLab CE and GitLab EE.

### 2. Explore GDK
Check [How to use GitLab Development Kit doc](https://gitlab.com/gitlab-org/gitlab-development-kit/blob/master/doc/howto/README.md) to get accustomed with GDK. 
You’ll need to know [commands](https://gitlab.com/gitlab-org/gitlab-development-kit/blob/master/HELP) to operate your local environment successfully. 
For example, to start database locally (to run tests), you need to start db with `gdk run db`.

_Quick tip:_ GDK contains collection of resources that help running an instance of GitLab locally as well as GitLab codebase itself. Code of EE or CE version can be found in `/gitlab` folder of GDK. 

### 3. Install CI Runner
Follow [this tutorial](https://gitlab.com/gitlab-org/gitlab-development-kit/blob/master/doc/howto/runner.md) to install GitLab Runner locally. 

_Quick tip:_ Register your runner by running `gitlab-runner register -c <gdk-path>/gitlab-runner-config.toml`, choose `Docker` as an executor, 
and run the GitLab Runner in the foreground with a command `gitlab-runner --log-level debug run --config <gdk-path>/gitlab-runner-config.toml`.

If you have questions about the runner or you're stuck and need help, ask in `#ci_runner` Slack channel.  

As well, consider reading and watching this workshop about setup Gitlab CI. It contains valuable info about Docker with Docker Machine and custom domain name. 
[Description](https://docs.google.com/document/d/1HnT2CCtaE7MMD7Qkd8_MLugwICDqu9Lp3f48o76Dzwk), [Video](https://drive.google.com/file/d/17JG1Tp6no6gea4y5CRbsXSNzNNHAlcFK/view)   

When you create your runner, make sure that you made it **privileged**. You need this setting to run "Docker in Docker". It's a requirement for secure jobs. 
[Read more about it](https://docs.gitlab.com/runner/executors/docker.html#use-docker-in-docker-with-privileged-mode)

### 4. Import test project
At this step you should have on your local machine: 
 * installed gdk and running a local instance of GitLab EE (gdk run),
 * running and connected to GitLab CI Runner.

On your local instance of GitLab, import by URL [test sast project](https://gitlab.com/gitlab-org/security-products/tests/sast). Add this project to some group. 
It will allow you to enable security dashboard correctly.

### 5. Run pipeline
Create and run a pipeline for `master` branch of the previously uploaded project. 

Sast job is using [GitLab SAST tool](https://gitlab.com/gitlab-org/security-products/sast/blob/master/README.md).
 
At this step, you may need tuning the runner with environment variables listed in the readme. For example, if you have an unstable internet connection, you may need to increase `SAST_PULL_ANALYZER_IMAGE_TIMEOUT`. 
Learn more about [SAST environment variables](https://gitlab.com/gitlab-org/security-products/sast#environment-variables). If you have questions, ask them in `#g_secure` Slack channel. 

As a result, you should have a green pipeline. On success, the pipeline should show the security report having uploaded as an artifact. This report will be used to populate the security dashboards.

### 6. Explore Security output
Go to the pipeline page and explore the “Security” tab in the pipeline, similar to [this pipeline](https://gitlab.com/gitlab-org/security-products/tests/sast/pipelines/36443854/security). When clicking on a vulnerability, GitLab shows a modal window with more information (like the file where the vulnerability has been found) and actions (dismiss and create an issue).

### 7. Improve this document
Create an MR with improvements to this document.

### 8. You are ready for new tasks
You're awesome!
